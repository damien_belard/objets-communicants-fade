int ledPin = 9;
int brightness = 5;
int fadeAmount = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(ledPin, brightness);
  brightness = brightness + fadeAmount;
  if(brightness >= 255){
    brightness = 5;
  }
  delay(60);
}
